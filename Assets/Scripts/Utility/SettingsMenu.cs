﻿/*The purpose of this script is to allow us to use an options menu in game
This will be active on the menu or in game
It will allow us to adjust the settings and save them*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SettingsMenu : MonoBehaviour 
{
    //Custom AudioMixer for adjusting the volume
	public AudioMixer audioMixer;
    public AudioManager audioManager;

    public static SettingsMenu instance;
    public Animator anim;

    //An array of resolutions for the player to choose from
    //PC ONLY
     Resolution[] resolutions;

    //This is the dropdown box that allows the user to select a resolution from the resolutions array
    public Dropdown resolutionDropdown;

    public Slider volumeSlider;
    public Toggle musicToggle;
    public Toggle fullscreenToggle;
    public GameObject creditsMenu;

    //PlayerPrefs
    string volumeKey = "GameVolume";
    //Default value of the volume is 0 in the volume settings
    float volumeValue;
    string fullscreenKey = "FullScreenKey";
    bool fullscreenValue = true;

    void Awake()
    {
        Debug.Log("The game has started and the audio is set");
        volumeValue = GetVolume();
        volumeSlider.value = volumeValue;
        //Debug.Log("Player prefs volume is: " + volumeValue);
        GetVolume();
        resolutions = Screen.resolutions;

        resolutionDropdown.ClearOptions();

        List<string> options = new List<string>();

        int currentResolutionIndex = 0;

        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + "x" + resolutions[i].height;
            options.Add(option);

            //Can't compare resolutions, so we have to see if their width and height match
            if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
            }
        }

        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();

        audioMixer.SetFloat("volume", volumeValue);
        SetFullScreen(GetFullScreen());
        HideCredits();

        this.gameObject.SetActive(false);
    }

    void Start()
	{
        
		resolutions = Screen.resolutions;
		
		resolutionDropdown.ClearOptions();
		
		List<string> options = new List<string>();
		
		int currentResolutionIndex = 0;
		
		for(int i = 0; i < resolutions.Length; i++)
		{
			string option = resolutions[i].width + "x" + resolutions[i].height;
			options.Add(option);
			
            //Can't compare resolutions, so we have to see if their width and height match
			if(resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
			{
				currentResolutionIndex = i; 
			}
		}

        resolutionDropdown.AddOptions(options);
		resolutionDropdown.value = currentResolutionIndex;
		resolutionDropdown.RefreshShownValue();

        audioMixer.SetFloat("volume", volumeValue);

        this.gameObject.SetActive(false);
        
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
        volumeValue = GetVolume();
        volumeSlider.value = volumeValue;
        //Debug.Log("Player prefs volume is: " + volumeValue);
        GetVolume();
        SetFullScreen(GetFullScreen());
        musicToggle.isOn = GetMusicEnabled();
        fullscreenToggle.isOn = GetFullScreen();
        //SetResolution(GetResolution());
    }

    void OnDisable()
    {
        //anim.SetTrigger("Disabled");
        PlayerPrefs.SetFloat(volumeKey, volumeValue);
        Debug.Log("Set the volume to :" + volumeValue);
    }
	
    //Each Set method is use to set whichever variable it controls

    //Sets the volume by taking in a float for the volume
    //and setting the Audio Mixers volume to the specified amount
    //from the Volume slider
	public void SetVolume(float volume)
	{
        Debug.Log(volume);
		audioMixer.SetFloat("volume", volume);
        volumeValue = volume;
        PlayerData.VolumeLevel = volume;
    }

    public float GetVolume()
    {
        Debug.Log("The volume amount is :" + volumeKey);
        return PlayerPrefs.GetFloat(volumeKey);
        
    }
	
    //This method sets the quality based on the quality setting that corrosponds to the int passed in
	public void SetQuality(int qualityIndex)
	{
		QualitySettings.SetQualityLevel(qualityIndex);
	}

    public bool GetFullScreen()
    {
        return PlayerPrefs.GetInt("FulLScreen") == 1 ? true : false;
    }

    //Sets the fullscreen toggle in the Screen Class
	public void SetFullScreen(bool isFullScreen)
	{
        Screen.fullScreen = isFullScreen;
        PlayerPrefs.SetInt("FulLScreen", isFullScreen ? 1 : 0);
        SetResolution(GetResolution());

        if (!isFullScreen)
        {
            Resolution resolution = Screen.currentResolution;
            Screen.SetResolution(resolution.width, resolution.height, isFullScreen);
        }

        Debug.Log("Fullscreen toggle is " + GetFullScreen());
    }

    public int GetResolution()
    {
        Debug.Log("Current resolution number is" + PlayerPrefs.GetInt("Resolution"));
        return PlayerPrefs.GetInt("Resolution");
        
    }

    //Takes the resolutions and adjusts based on the selected resolution
    //Creates a temp resolution and assigns the current resolutions to its value
    public void SetResolution(int resolutionIndex)
	{
		//Resolution resolution = resolutions[resolutionIndex];
		Screen.SetResolution(resolutions[resolutionIndex].width, resolutions[resolutionIndex].height, Screen.fullScreen);
        PlayerPrefs.SetInt("Resolution", resolutionIndex);
	}

    public void ToggleAudio(bool audioEnabled)
    {
        PlayerPrefs.SetInt("MusicEnabledInt", audioEnabled ? 1 : 0);
        audioManager.audioEnabled = audioEnabled;
        audioManager.EnableAudio(audioEnabled);
        Debug.Log("Is music enabled? = " + PlayerPrefs.GetInt("MusicEnabledInt"));
    }

    public bool GetMusicEnabled()
    {
        return PlayerPrefs.GetInt("MusicEnabledInt") == 1 ? true : false;
    }


    //Find the Audio Manager when this object is enabled
    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        Debug.Log(GameObject.Find("_AudioManager") + " was found!");
        audioManager = GameObject.Find("_AudioManager").GetComponent<AudioManager>();        
        SetFullScreen(GetFullScreen());
        SetResolution(GetResolution());
        Debug.Log("Fullscreen toggle is " + PlayerPrefs.GetInt("FulLScreen"));
    }


    public void DisplayCredits()
    {
        creditsMenu.SetActive(true);
    }

    public void HideCredits()
    {
        creditsMenu.SetActive(false);
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            //Display a quit message
        }

        if(Input.GetKeyDown(KeyCode.A))
        {
            SetResolution(0);
        }
    }

}